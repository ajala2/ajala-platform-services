
#### Overview
This folder contains simple client's for ajala's ASR REST service.

Your use of ajala's ASR services is subject to ajala's [Terms & Conditions](https://www.ajala.ai/terms-of-service), [Privacy Policy](https://www.ajala.ai/privacy-policy), and any other terms and disclosures available on [ajala.ai](https://www.ajala.ai).
<br>
<br>

**HTTP Client: https**
<br>
`sample_http_asr_client.py` shows a sample script for posting an authenticated request to ajala's HTTPS ASR service to transcribe an audio file.

We recommend using the HTTP service for **short duration** (<5min) audio files. 
<br>
<br>


**Websocket Client: wss**
<br>
`sample_websocket_asr_client.py` shows a sample web socket client for connecting to ajala's Websocket ASR service.

We recommend using the Websocket service for **long duration** audio files, or applications that require maintaining a long-running connection to the service.
<br>
<br>
<br>

#### Status Codes
| Status Code | Description  |
|--|--|
| 200 | Request succesful  |
| 401 | Unauthorised request (invalid authentication key)  |
| 400 | Bad Request  |
| 415 | Unsupported Media Type |
| 500 | Internal Server Error  |
| 503 | Service Unavailable  |
<br>
<br>
<br>

Platform [Documentation](https://api.ajala.ai/documentation#quickTutorialSTT) provides sample response formats. 

Platform access requires a valid ajala user account. Please contact [ajala-support](support@ajalaco.com) for further details.
