import ssl
import json
import time
import asyncio
import pathlib
import websockets

uri = "wss://api.ajala.ai/asr/ws/client"
headers = {'content-type':'audio/wav','device-id':'test_device','recognizer-id':'engGH16k','apikey':'<API_KEY>'}


upload_byterate = 8000
audio_file = "../audio/gheng_test.wav"
pem_file = "<ajala_pem_file>"

ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
ajala_pem = pathlib.Path(__file__).with_name(pem_file)
ssl_context.load_verify_locations(ajala_pem)


async def asr_app():
   print("Connecting to websocket server...")
   async with websockets.connect(uri,ssl=ssl_context,extra_headers=headers) as ws:
      print("Connected to websocket server!")
      
      print("Uploading data...")
      with open(audio_file,'rb') as audio_data:
         chunk_count=1
         for chunk in iter(lambda: audio_data.read(upload_byterate),""):
            if len(chunk)==0:
               break
            else:
               await ws.send(chunk)
               print(f"{chunk_count}: Sent chunk of length {len(chunk)} to websocket")
               time.sleep(1.0/5)
               chunk_count+=1
      print("Finished sending audio data, sending EOS")
      await ws.send("EOS")
      print("EOS. Awaiting response from api.ajala.ai...")

      response = await ws.recv()
      print(response)

if __name__=="__main__":
   asyncio.run(asr_app())


