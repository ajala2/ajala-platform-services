import requests

url = "https://api.ajala.ai/asr/dynamic/client"
headers = {"apikey":"<API_KEY>","device-id":"<device_name>","recognizer-id":"engGH16k","Content-type":"audio/wav"}
audio_file = "../audio/gheng_test.wav"
pem_path = "/path/to/ajala/ssl/pem/file.pem"

try:
   with open(audio_file,'rb') as audio_data:
      request = requests.post(url,headers=headers,data=audio_data,verify=pem_path)
except Exception as e:
   print(e)
else:
   print(request.status_code)
   print(request.content)



