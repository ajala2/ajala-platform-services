# ajala Platform Services



## Demos - [ajala](https://www.ajala.ai) REST services

Sample scripts for executing requests against ajala's REST API

For support, please conntact [support@ajalaco.com](support@ajalaco.com)!


## License

Copyright 2022 AJA.LA STUDIOS LTD


    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
